require 'redis'
require 'json'


$redis = Redis.new(:timeout=>0)

$redis.subscribe('message_recorder') do |on|
 on.message do |channel, msg|
	data = JSON.parse(msg)

	puts "##{channel} - [#{data['id']}]: #{data['end_reason']  }  #{data['name']}   #{data['call_duration']}  "

 end


end
