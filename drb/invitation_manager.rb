require 'ruby_events/core'
require 'rest_client'
#require 'pusher'
require 'celluloid'
require_relative '../media/aws.rb'
require_relative '../tts/ivona.rb'
require 'redis'
require 'json'
require 'digest/sha1'
require 'digest/md5'
require 'net/http'
require 'fileutils'

class DialWithAsyncRecord < Adhearsion::CallController
	def run
		answer
		logger.info self.inspect
		record_attr = (!metadata["child"].attributes["record"].nil? && metadata["child"].attributes["record"].value.to_s.strip != "") ? metadata["child"].attributes["record"].value.to_s.strip : "missing"
		if record_attr.downcase=='true' && record_attr!="missing"
			logger.info "Dial is recording enabled"
			$async_record_start_time = Time.now.to_i
			record async: true  do |event|
				recordurl = "#{SINATRA_URL}/media/"+event.recording.uri.match(/[^\/]*$/)[0].to_s
				@smscresponse= {"calluid"=>metadata["metadata"]["calluid"] ,
								"from"=> metadata["metadata"]["from"],
								"to"=>metadata["metadata"]["to"] ,
								"event"=>"async_record" ,
								"recordurl"=> recordurl ,
								"starttime"=>$async_record_start_time,
								"endtime"=>Time.now.to_i,
								"callstatus" => "inprogress",
								"callee"=>metadata["child"].text,
								"caller"=>(!metadata["child"].attributes["from"].nil? && metadata["child"].attributes["from"].value.to_s.strip != "") ? metadata["child"].attributes["from"].value.to_s : metadata["metadata"]["from"],
								"direction"=>"outbound" }
				async_record_action_url = (!metadata["child"].attributes["action"].nil? && metadata["child"].attributes["action"].value.to_s.strip != "") ? metadata["child"].attributes["action"].value : metadata["metadata"]["callback_url"]
				async_record_action_method = (!metadata["child"].attributes["method"].nil? && metadata["child"].attributes["method"].value.to_s.strip != "") ? metadata["child"].attributes["method"].value : metadata["metadata"]["default_method"]
				@restclient = RestClient::Resource.new(async_record_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
				logger.info "posting for async recording  #{@smscresponse}"
				case async_record_action_method.downcase
				when 'post'					
					begin
						@call_back_response =  @restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
						report_call(metadata["metadata"]["calluid"],metadata["metadata"]["to"],"async_record", async_record_action_url, @smscresponse, @call_back_response)
					rescue Exception => e
						logger.error "Error while post async_record to #{async_record_action_url}"
						logger.error e
						logger.error e.backtrace
						report_call(metadata["metadata"]["calluid"],metadata["metadata"]["to"],"async_record", async_record_action_url, @smscresponse, e)
						hangup
					end
				when 'get'
					begin
						@smscresponse = construct_get_params(@smscresponse)
						@call_back_response =   @restclient.get :params => @smscresponse
						report_call(metadata["metadata"]["calluid"],metadata["metadata"]["to"],"async_record", async_record_action_url, @smscresponse, @call_back_response)
					rescue Exception => e
						logger.error "Error while post async_record to #{async_record_action_url}"
						logger.error e
						logger.error e.backtrace
						report_call(metadata["metadata"]["calluid"],metadata["metadata"]["to"],"async_record", async_record_action_url, @smscresponse, e)
						hangup
					end
				end				
			end
		end
	end
end

class InvitationManager  
	@@gateway =SIP_TRUNK 
	@@repo_dir = RECORDING_REPO
	@@template_dir = TEMPLATE_DIR
	@@app_dir = ADHEARSION_APP_ROOT 
	@@lib_dir = @@app_dir<<'/lib'
	@@ask_timeout = 100
	@@menu_timeout = 60 #seconds
	@@menu_tries = 3
	@@aws = Aws.new()
	#Pusher.app_id = PUSHER_APP_ID 
		#Pusher.key = PUSHER_KEY
	#Pusher.secret = PUSHER_SECRET
	def get_gateway_prefix(contact_type, contact_no)
        if(contact_type=='pstn')
                @ret = SIP_TRUNK 
        elsif(contact_type=='sip')
                @ret = 'user/'
        end
		if(contact_no[0] == '0' && contact_no[1]== '0')
		#do nothing
		elsif(contact_no[0] == '+')
		#do nothing
		elsif(contact_no[0] != '0' && contact_type == 'pstn' )
                @ret += '0'
        end
        @ret
    end
	def  send_invitation(contact_type , contact_no,invitation_audio_file,session_secret, callback_url,metadata )	
		metadata = JSON.parse(metadata)
		start_time = Time.now.to_i
		logger.info "outgoing call to #{metadata["to"]} from #{metadata["from"]}"	
		def is_this_a_missed_call(xml)   
			logger.info "missed call diagnostic"
			@doc = Nokogiri::XML(xml)						
			@doc.first_element_child.children.each do |child|			
				if(child.name == 'to' || child.name =='from' || child.name == 'callback_url' || child.name == 'text' )
					child.remove
				end
			end
			@remaining = @doc.first_element_child.children
			logger.info "remaining is #{@remaining}"				
			if(@remaining.length == 1 && @remaining.first.name == 'hangup')
				return (!@remaining.first.attributes["duration"].nil? && @remaining.first.attributes["duration"].to_s.strip != "") ? @remaining.first.attributes["duration"].value.to_s.to_i : 1						
			else
				return nil
			end
		end		
		@delay = is_this_a_missed_call(metadata["xml"])
		if (!@doc.first_element_child.attributes["method"].nil? && @doc.first_element_child.attributes["method"].value.to_s.strip != "")								
			metadata["default_method"] = @doc.first_element_child.attributes['method'].value.to_s.strip
		else
			metadata["default_method"] = "post"
		end
		if  !@delay.nil?
			metadata["missed_call"] =  true		   	
			logger.info "delay is #{@delay}"
			metadata["missed_call_duration"] = @delay.to_s.to_i 
		else
			metadata["missed_call"] = false
		end
		outbound_call =  Adhearsion::OutboundCall.originate "#{get_gateway_prefix(contact_type,metadata["to"]) + metadata["to"]}" , from:metadata['from'] do
			begin
				answering_time = 0
				start_time = Time.now.to_i
	        	$redis = Redis.new	
				if metadata["missed_call"]
					##  hangup ultimately
					logger.info "missed call attempt ---> #{metadata['to']}"
					$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<missed call>"})
				else		
					answer
					#passed copy of function
			 		def get_gateway_prefix(contact_type, contact_no)
        				if(contact_type=='pstn')
                			@ret = SIP_TRUNK
        				elsif(contact_type=='sip')
                			@ret = 'user/'
        				end
                		if(contact_no[0] == '0' && contact_no[1]== '0')
                			#do nothing
                		elsif(contact_no[0] == '+')
                			#do nothing
                		elsif(contact_no[0] != '0' && contact_type == 'pstn' )
                			@ret += '0'
        				end
        				@ret
    				end
					def local_audio_copy(invitation_audio_file)
						_file_name = invitation_audio_file
						_file_extension = invitation_audio_file.match(/[^.]+$/)[0]
						if ( invitation_audio_file.include? REPO_IP) # skipping downloading recording file available on this server						
							_file_name = invitation_audio_file.match(/[^\/]*$/).to_s
							logger.info("audio file available in media repository  #{_file_name}")
						elsif ( invitation_audio_file.include? "http://")							
							_file_name = invitation_audio_file.gsub(/[^a-zA-Z\.0-9]+/, '_')
							if (_file_extension.downcase!='au' && _file_extension.downcase!='mp3'  )
								if !(File.exist?(@@repo_dir + _file_name))
									system("wget -O #{@@repo_dir}#{_file_name}  #{invitation_audio_file  }")
								end
							else
								_file_name_wav = _file_name.gsub(/[^\.]+$/,'wav')
								if !(File.exist?(@@repo_dir + _file_name_wav))
									system("wget -O #{@@repo_dir}#{_file_name}  #{invitation_audio_file  }")
									system("sox #{@@repo_dir}#{_file_name} -t wav  -r 8000 -e mu-law   --channels 1 #{@@repo_dir}#{_file_name_wav}")
								end
								system("rm -f #{@@repo_dir}#{_file_name}")
								_file_name = _file_name_wav
							end
						end
						return _file_name
					end
					def local_tts_copy(txt,male_female)
						_file_name =    Digest::SHA1.hexdigest(txt)  + ".mp3"
						if !(File.exist?(@@repo_dir + _file_name))
							#ivona = Ivona.new("/var/punchblock/record/")
							#ivona.text_to_ogg(txt,male_female,_file_name)
							_engine_id = "3"
							_language_id = "1"
							_voice_id = "3"
							_account = "3623895"
							_api_key = "2307281"
							_secret_phrace = "abb3285edad9861b34074386fc1eb8c5"
							_cs = ""
							_post_md5 = ""
							_base_url = "http://cache.oddcast.com/tts/gen.php"
							_post_info = ""
							_post_info = "?EID=" + _engine_id.to_s + "&LID=" + _language_id.to_s + "&VID=" + _voice_id.to_s + "&TXT=" + URI.encode(txt).to_s + "&ACC=" + _account.to_s + "&API=" + _api_key.to_s
							_post_md5 = _engine_id.to_s + _language_id.to_s + _voice_id.to_s + txt.to_s + _account.to_s + _api_key.to_s + _secret_phrace.to_s
							_cs = Digest::MD5.hexdigest(_post_md5) #"3" + "1" + "3" + "This is for a sample test from madhu please ignore it." + "3623895" + "2307281" + "abb3285edad9861b34074386fc1eb8c5"        
							_post_info = _post_info + "&CS=" + _cs
							begin
								logger.info "vocalware api call start"
								uri = URI(_base_url + _post_info)
								res = Net::HTTP.get_response(uri)
								logger.info "vocalware api call end"
								File.new(@@repo_dir + _file_name, "w+")
								File.open(@@repo_dir + _file_name, 'w') { |file| file.write(res.body) }
								logger.info "tts saved to " + @@repo_dir +  _file_name
							rescue Exception => e
								logger.info "Exception in vocalware api call"
								logger.info e.backtrace
							end
						end
						logger.info "internal tts job done #{_file_name}"
						return _file_name
					end		
					answering_time = Time.now.to_i
					data = {"name"=>"at_answer","id"=>outbound_call.id ,   "answering_time"=>answering_time }
					$redis = Redis.new
					$redis.publish "outbound_dialer", data.to_json
					@xml = metadata["xml"] 
					logger.info "#######################################################"+metadata["xml"]
					while(true)
						@smscresponse = nil
						logger.info "process xml data ========================== #{@xml}"
						_xml = Nokogiri::XML(@xml)
						@xml = nil #consume xml		
						if(_xml.first_element_child().nil?)
							break
						else
							metadata["enclosure"] = _xml.first_element_child().name
							metadata["attributes"] = _xml.first_element_child().attributes							
							c = _xml.first_element_child().children()
							_recordcall = 'false'
							if !metadata["attributes"]["recordcall"].nil?
								_recordcall = metadata["attributes"]["recordcall"].value
							end	
							# call recording w.r.t. recordcall instruction in request
							if metadata["enclosure"].strip.downcase =='request' && _recordcall == 'true'
								logger.info "Request is recording enabled"
								$async_record_start_time = start_time 
								record async: true  do |event|
									recordurl = "#{SINATRA_URL}/media/"+event.recording.uri.match(/[^\/]*$/)[0].to_s
									@smscresponse= {"calluid"=>metadata["calluid"] , "from"=> metadata["from"], "to"=>metadata["to"] ,  "event"=>"call_record" ,"recordurl"=> recordurl ,    "starttime"=>$async_record_start_time, "endtime"=>Time.now.to_i,"callstatus" => "inprogress",   "direction"=>"outbound" }
									if ( !metadata["attributes"]["action"].nil? && metadata["attributes"]["action"].value!='')
										action_url = metadata["attributes"]["action"].value
									else
										action_url = metadata["callback_url"]
									end
									@restclient = RestClient::Resource.new(action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)									
									case metadata['default_method'].downcase
									when 'post'											
											begin
												@call_back_response = @restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
											rescue Exception => e
												logger.error "Error while porting request record to #{action_url}"
												logger.error e
												logger.error e.backtrace
												hangup
											end
									when 'get'
										begin
											@smscresponse = construct_get_params(@smscresponse)
											@call_back_response =   @restclient.get :params => @smscresponse
										rescue Exception => e
											logger.error "Error while porting request record to #{action_url}"
											logger.error e
											logger.error e.backtrace
											hangup
										end
									end
								end
							end
							###############################################
							c.each do |child|				
								if(child.name== 'hangup')
									logger.info "hangup..................................................."
									$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<hangup>"})
									hangup
									break
								end				
								if(child.name== 'play')
									logger.info "play......................................................#{child.text}"
									play @@repo_dir+local_audio_copy(child.text)
						 			$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<play>"})
								end				
								if(child.name== 'speak')
									logger.info "speak....................................................."
									play @@repo_dir+local_tts_copy(child.text, 'female')
						 			$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<speak>"})
								end				
								if(child.name== 'dial')
									logger.info "dial......................................................"
						 			$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<dial>"})
									dial_start_time = Time.now.to_i
									moh_clip = (!child.attributes["onholdmusic"].nil? && child.attributes["onholdmusic"].value.to_s.strip != "") ? @@repo_dir + local_audio_copy(child.attributes["onholdmusic"].value) : @@repo_dir + "moh.wav"
									moh = play! moh_clip
									call.on_joined { moh.stop! }
									destination=[]
									child.text.split(",").each do |dest|
										destination.push "#{get_gateway_prefix('pstn',dest.strip) + dest.strip}"
									end
									dial_timeout = (!child.attributes["timeout"].nil? && child.attributes["timeout"].value.to_s.strip != "") ? child.attributes["timeout"].value.to_s.to_i : 30
									dial_from = (!child.attributes["from"].nil? && child.attributes["from"].value.to_s.strip != "") ? child.attributes["from"].value : metadata["from"]
									dial_action_url = (!child.attributes["action"].nil? && child.attributes["action"].value.to_s.strip != "") ? child.attributes["action"].value : metadata["callback_url"]
									dial_action_method = (!child.attributes["method"].nil? && child.attributes["method"].value.to_s.strip != "") ? child.attributes["method"].value : metadata["default_method"]
									status = dial destination, for: dial_timeout.to_i.seconds , confirm: DialWithAsyncRecord , confirm_metadata:{"child"=>child, "metadata"=>metadata} , from: dial_from
									case status.result
										when :answer
							 				$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<dial>:<answer>"})
							  				@smscresponse= {"calluid"=>metadata["calluid"] ,
							  								"from"=> metadata["from"],
							  								"to"=>metadata['to'] ,
							  								"event"=>"dial",
							  								"status"=>"answer",
							  								"async_record"=>'true',
							  								"starttime"=>dial_start_time,
							  								"endtime"=>Time.now.to_i,
							  								"callstatus" => "inprogress",
							  								"callee"=>status.calls[0].to.match(/[^\/]*$/)[0],
							  								"caller"=> dial_from,
							  								"direction"=>"inbound"
							  							}						
							 			when :error, :timeout, :no_answer
							 				$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<dial>:<#{status.result.to_s}>"})
											@smscresponse= {"calluid"=>metadata["calluid"] ,
															"from"=> metadata["from"],
															"to"=>metadata["to"] , 
															"event"=>"dial",
															"status"=>status.result.to_s,
															"async_record"=>"true",
															"callstatus" => "inprogress",
															"callee"=>child.text,
															"caller"=>dial_from,
															"direction"=>"inbound"
														}
										end
										case dial_action_method.downcase
										when 'post'
											logger.info "posting for dial  #{@smscresponse}"
											begin
												@restclient = RestClient::Resource.new(dial_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)						
												@call_back_response = @restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
												report_call(metadata["calluid"],metadata["to"],"dial", dial_action_url, @smscresponse, @call_back_response)
											rescue Exception => e
												logger.error "Error while posting dial to #{dial_action_url}"
												logger.error e
												logger.error e.backtrace
												report_call(metadata["calluid"],metadata["to"],"dial", dial_action_url, @smscresponse, e)
												hangup
											end
										when 'get'
											begin
												@restclient = RestClient::Resource.new(dial_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
												@smscresponse = construct_get_params(@smscresponse)
												@call_back_response = @restclient.get :params => @smscresponse
												report_call(metadata["calluid"],metadata["to"],"dial", dial_action_url, @smscresponse, @call_back_response)
											rescue Exception => e
												logger.error "Error while posting dial to #{dial_action_url}"
												logger.error e
												logger.error e.backtrace
												report_call(metadata["calluid"],metadata["to"],"dial", dial_action_url, @smscresponse, e)
												hangup
											end
										end
										logger.info "response for dial  #{@call_back_response}"
										if !@call_back_response.nil? && @call_back_response.to_s.strip!=''
											logger.info "DIAL RESPONSE IS NOT NIL OR BLANK"
											@xml = @call_back_response
										break
									end
								end #dial child
								if(child.name=='getkeys')
									begin
										# processing speak and play
										ask_announcement = []
										ask_timeout={}
										logger.info "getkey xml tree ------------> #{child}"
						 				$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<getkeys>"})
										child.children.each do |grand_child|						
											if(grand_child.name== 'play')
												logger.info "play......................................................#{child.text}"
									 			$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<getkeys>:<play>"})
												ask_announcement.push @@repo_dir+local_audio_copy(grand_child.text)
											end
											if(grand_child.name== 'speak')
												logger.info "speak....................................................."
												$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<getkeys>:<speak>"})
												ask_announcement.push @@repo_dir+local_tts_copy(grand_child.text, 'female')
											end
											if(grand_child.name== 'timeout')
												ask_timeout = { "count" => (!grand_child.attributes["count"].nil? && grand_child.attributes["count"].to_s != "") ? grand_child.attributes['count'].value.to_s.to_i : 1,
																"type" =>   grand_child.attributes['type'].value ,
																"content" => grand_child.text
															}							
											end					
										end
										max_keys = (!child.attributes["max"].nil? && child.attributes["max"].value.to_s.strip != "") ? child.attributes["max"].value.to_s.to_i : 1
										keys_terminator = (!child.attributes["tkey"].nil? && child.attributes["tkey"].value.to_s.strip != "") ? child.attributes["tkey"].value : "#"
										valid_keys = (!child.attributes["validkeys"].nil? && child.attributes["validkeys"].value.to_s.strip != "") ? child.attributes["validkeys"].value : "0123456789*#"
										keys_timeout = (!child.attributes["timeout"].nil? && child.attributes["timeout"].to_s.strip != "") ? child.attributes["timeout"].value.to_s.strip.to_i : 10									
										getkeys_action_url = (!child.attributes["action"].nil? && child.attributes["action"].value.to_s.strip != "") ? child.attributes["action"].value.to_s.strip : metadata["callback_url"]
										getkeys_action_method = (!child.attributes["method"].nil? && child.attributes["method"].value.to_s.strip != "") ? child.attributes["method"].value.to_s.strip : metadata["default_method"]
										#ask_announcement = ask_announcement.push  #",#{@@template_dir}beep.mp3"				
										##########################################
										logger.info "getkeys...................................................Max Keys : #{max_keys} , Terminator : #{keys_terminator}    announcements are #{ask_announcement}  valid keys => #{valid_keys}"
										limit=child.attributes["max"].value.to_s.to_i
										terminator=child.attributes["tkey"].value.to_s
										logger.info "timeout parameters are #{ask_timeout}" 
										for i in 1..((ask_timeout.nil?) ? 1 : ask_timeout["count"] ).to_i				
											logger.info "getkeys attempt  -------  #{i} -------"
										 	$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<getkeys>:<attempt>:#{i}"})
											ask_result = ask ask_announcement, :limit=>max_keys, :terminator=>keys_terminator, :timeout=>keys_timeout					#do |buffer|
											#logger.info "getkey buffer is --------------> #{buffer}"
											#if !child.attributes['validkeys'].include? buffer.to_s[-1,1]
											#	buffer =buffer.slice(0,buffer.length - 2)
											#end
											#end
											if ask_result.nil? && ask_result.response.strip==''
												if !ask_timeout.nil?					 
										   			$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<getkeys>:<timeout>"})			
													ask_timeout["type"]=='speak' ? (play @@repo_dir+local_tts_copy(ask_timeout["content"], 'female')) : (play @@repo_dir+local_audio_copy(ask_timeout["content"]))
												end
											else
												break
											end
										end
										logger.info "getkeys result is #{ask_result.nil?}"
										_res = (!ask_result.nil? && !ask_result.response.nil?) ? ask_result.response : ""
										if _res != ""
											@smscresponse= {"calluid"=>metadata["calluid"],
															"from"=> metadata["from"],
															"to"=> metadata["to"],
															"event"=>"getkeys",
															"callstatus" => "inprogress",
															"digits"=>ask_result.response,
															"direction"=>"outbound"
														}
											logger.info "getkeys response --------------> #{@smscresponse}"
											case getkeys_action_method.downcase
											when 'post'
												begin
													@restclient = RestClient::Resource.new(getkeys_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)						
													@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
													report_call(metadata["calluid"],metadata["to"],"getkeys", getkeys_action_url, @smscresponse, @call_back_response)
												rescue Exception => e
													logger.error "Error while posting getkeys to #{getkeys_action_url}"
													logger.error e
													logger.error e.backtrace
													report_call(metadata["calluid"],metadata["to"],"getkeys", getkeys_action_url, @smscresponse, e)
													hangup
												end
											when 'get'
												begin
													@restclient = RestClient::Resource.new(getkeys_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
													@smscresponse = construct_get_params(@smscresponse)
													@call_back_response = @restclient.get :params => @smscresponse
													report_call(metadata["calluid"],metadata["to"],"getkeys", getkeys_action_url, @smscresponse, @call_back_response)
												rescue Exception => e
													logger.error "Error while posting getkeys to #{getkeys_action_url}"
													logger.error e
													logger.error e.backtrace
													report_call(metadata["calluid"],metadata["to"],"getkeys", getkeys_action_url, @smscresponse, e)
													hangup
												end
											end
										end
										#	@call_back_response = JSON.parse(@call_back_response)
										logger.info "Response from getkeys callback #{@call_back_response}"
										if !@call_back_response.nil? && @call_back_response.to_s.strip != ""
											@xml = @call_back_response
											break
										end
									rescue Exception=> e
										logger.error "Error while posting getkeys to #{getkeys_action_url}"
										logger.error e
										logger.error e.backtrace
										report_call(metadata["calluid"],metadata["to"],"getkeys", getkeys_action_url, @smscresponse, e)
										hangup
									end
								end #getkey child				
								if(child.name == 'record')
									logger.info "record.................................................... "					
						 			$redis.publish "task_manager",JSON.pretty_generate(  {"calluid"=>metadata["calluid"],"msg"=>"<record>"})
						 			record_action_url = (!child.attributes["action"].nil? && child.attributes["action"].value.to_s.strip != "") ? child.attributes["action"].value.to_s.strip : metadata["callback_url"]
						 			record_action_method = (!child.attributes["method"].nil? && child.attributes["method"].value.to_s.strip != "") ? child.attributes["method"].value.to_s.strip : metadata["default_method"]
						 			max_length = (!child.attributes["maxlength"].nil? && child.attributes["maxlength"].value.to_s.strip != "") ? child.attributes["maxlength"].value.to_s.strip.to_i : 30
						 			record_terminator = (!child.attributes["finishonkey"].nil? && child.attributes["finishonkey"].value.to_s.strip != "") ? child.attributes["finishonkey"].value.to_s.strip : "#"
									play @@template_dir+"long_beep.mp3"
									record_start = Time.now.to_i
									record_result = record :max_duration=>max_length, :interruptible=>record_terminator
									record_end = Time.now.to_i
									@smscresponse= {"calluid"=>metadata["calluid"],
													"from"=> metadata["from"] ,
													"to"=> metadata["to"],
													"event"=>"record",
													"callstatus" => "inprogress",
													"recordurl"=>"#{SINATRA_URL}/media/"+record_result.recording_uri.match(/[^\/]*$/).to_s, 
													"duration"=>(record_end - record_start).to_s,
													"direction"=>"outbound"
												}					
									logger.info "record result saved in #{record_result.recording_uri.match(/[^\/]*$/).to_s}"
									case record_action_method.downcase
									when 'post'
										begin
											@restclient = RestClient::Resource.new(record_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
											@call_back_response = @restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
											report_call(metadata["calluid"],metadata["to"],"record", record_action_url, @smscresponse, @call_back_response)
										rescue Exception => e
											logger.error "Error while posting record to #{record_action_url}"
											logger.error e
											logger.error e.backtrace
											report_call(metadata["calluid"],metadata["to"],"record", record_action_url, @smscresponse, e)
											hangup
										end
									when 'get'
										begin
											@restclient = RestClient::Resource.new(record_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)											
											@smscresponse = construct_get_params(@smscresponse)
											@call_back_response = @restclient.get :params => @smscresponse
											report_call(metadata["calluid"],metadata["to"],"record", record_action_url, @smscresponse, @call_back_response)
										rescue Exception => e
											logger.error "Error while posting record to #{record_action_url}"
											logger.error e
											logger.error e.backtrace
											report_call(metadata["calluid"],metadata["to"],"record", record_action_url, @smscresponse, e)
											hangup
										end					
									end
							 		logger.info "Response from record callback #{@call_back_response}"
									if !@call_back_response.nil? && @call_back_response.to_s.strip != ""
										@xml = @call_back_response
										break						
									end
								end # Record Child
							end # xml not nil
							if(@xml.nil? && metadata["enclosure"]=='request')
								logger.info "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ #{metadata}"
								@smscresponse= {"calluid"=>metadata["calluid"],
												"from"=> metadata["from"],
												"to"=> metadata["to"],
												"event"=>"newcall",
												"callstatus" => "received",
												"direction"=>"outbound"
											}
								case metadata["default_method"].downcase
								when 'post'
									begin
										@restclient = RestClient::Resource.new(metadata["callback_url"], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
										@call_back_response = @restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
										report_call(metadata["calluid"],metadata["to"],"newcall", metadata["callback_url"], @smscresponse, @call_back_response)
									rescue Exception => e
										logger.error "Error while posting newcall to #{metadata['callback_url']}"
										logger.error e
										logger.error e.backtrace
										report_call(metadata["calluid"],metadata["to"],"newcall", metadata["callback_url"], @smscresponse, e)
										hangup
									end
								when 'get'
									begin
										@restclient = RestClient::Resource.new(metadata["callback_url"], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
										@smscresponse = construct_get_params(@smscresponse)
										@call_back_response = 	@restclient.get :params => @smscresponse
										report_call(metadata["calluid"],metadata["to"],"newcall", metadata["callback_url"], @smscresponse, @call_back_response)
									rescue Exception => e
										logger.error "Error while posting newcall to #{metadata['callback_url']}"
										logger.error e
										logger.error e.backtrace
										report_call(metadata["calluid"],metadata["to"],"newcall", metadata["callback_url"], @smscresponse, e)
										hangup
									end					
								end								
								if !@call_back_response.nil? && @call_back_response.to_s.strip != ""
									@xml = @call_back_response
									logger.info "XML recieved in new-call response : #{@xml}"
								else						
									break
								end
							elsif(@xml.nil?)
								break
							end
						end#null xml
					end#while end
					hangup	
				end #non missed call scenario
				hangup # simply hangup immediately for missed call
    		rescue Exception => e # outbound originate rescue
				logger.info "EXCEPTION raised during OBD task #{e}"				
				report_call(metadata["calluid"],metadata["to"],"error", metadata["callback_url"], @smscresponse, e)
				#	$redis.publish "outbound_dialer" ,({"name"=>"exception","id"=>outbound_call.id , "where"=>"obd","exception"=> "!!! Exception raise during outbound originate -> #{e} "}).to_json
			end  # rescue end
		end # outbound originate end
		outbound_call.on_end do
    		begin	
				end_time = Time.now.to_i
				release_reason = outbound_call.end_reason.to_s
				end_reason = release_reason.gsub(/__.*/,'')
				gateway_reason = release_reason.gsub(/.*__/,'')
				logger.info("call has ended and end reason was #{end_reason}")
	    		$redis = Redis.new
        		data = {"name"=>'at_end', "start_time"=> start_time,"end_time"=>end_time  , "id"=>outbound_call.id, "end_reason"=>end_reason , "gateway_reason"=>gateway_reason}
        		$redis.publish "outbound_dialer", data.to_json
				#Pusher['360invite'].trigger('at_invitation_end',data.merge( { :contact_no => contact_no, :session_secret =>session_secret }  ))
				if metadata["missed_call"]
					@smscresponse= {"calluid"=>metadata["calluid"],
									"from"=> metadata["from"] ,
									"to"=> metadata["to"],
									"event"=>"missed_call_hangup",
									"callstatus" => "completed", 
									"direction"=>"outbound" ,
									"starttime"=>start_time,
									"endtime"=>end_time 
								}
					hangup_elm = @doc.first_element_child.children.first
					missed_call_hangup_action_url = (!hangup_elm.attributes['action'].nil? && hangup_elm.attributes['action'].value.to_s.strip != "") ? hangup_elm.attributes['action'].value.to_s.strip : metadata['callback_url']
					missed_call_hangup_action_method = (!hangup_elm.attributes['method'].nil? && hangup_elm.attributes['method'].value.to_s.strip != "") ? hangup_elm.attributes['method'].value.to_s.strip : metadata['default_method']
					@doc.first_element_child.children.first.attributes['']
					case missed_call_hangup_action_method
					when 'post'
						begin
							@restclient = RestClient::Resource.new(missed_call_hangup_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)	
							@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
							report_call(metadata["calluid"],metadata["to"],"missed_call_hangup", missed_call_hangup_action_url, @smscresponse, @call_back_response)
						rescue Exception => e
							logger.error "Error while posting missed_call_hangup to #{missed_call_hangup_action_url}"
							logger.error e
							logger.error e.backtrace
							report_call(metadata["calluid"],metadata["to"],"missed_call_hangup", missed_call_hangup_action_url, @smscresponse, e)
						end
					when 'get'
						begin
							@restclient = RestClient::Resource.new(missed_call_hangup_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)	
							@smscresponse = construct_get_params(@smscresponse)							
							@call_back_response = 	@restclient.get :params => @smscresponse
							report_call(metadata["calluid"],metadata["to"],"missed_call_hangup", missed_call_hangup_action_url, @smscresponse, @call_back_response)
						rescue Exception => e
							logger.error "Error while posting missed_call_hangup to #{missed_call_hangup_action_url}"
							logger.error e
							logger.error e.backtrace
							report_call(metadata["calluid"],metadata["to"],"missed_call_hangup", missed_call_hangup_action_url, @smscresponse, e)
						end						
					end					
				else
					logger.info "hangup event generation"
					logger.info metadata.inspect
					callstatus = ''
					if(end_reason=='hangup'|| end_reason=='hangup_command')
						_callstatus = 'completed'
					elsif(end_reason=='busy')
						_callstatus = 'no-answer'
					elsif(end_reason=='reject')
						_callstatus = 'cancel'
					elsif(end_reason=='error')
						_callstatus = 'failed'
					elsif(end_reason=='timeout')
						_callstatus = 'timeout'									
					end	
					@smscresponse= {"calluid"=>metadata["calluid"],
									"from"=> metadata["from"] ,
									"to"=> metadata["to"],
									"event"=>"hangup",
									"callstatus" => _callstatus,
									"endreason"=>gateway_reason ,
									"direction"=>"outbound" ,
									"starttime"=>start_time,
									"endtime"=>end_time
								}
					case metadata['default_method'].downcase
					when 'post'
						begin
							@restclient = RestClient::Resource.new( metadata["callback_url"], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
							@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
							report_call(metadata["calluid"],metadata["to"],"hangup", metadata["callback_url"], @smscresponse, @call_back_response)
						rescue Exception => e
							logger.error "Error while posting hangup to #{metadata['callback_url']}"
							logger.error e
							logger.error e.backtrace
							report_call(metadata["calluid"],metadata["to"],"hangup", metadata["callback_url"], @smscresponse, e)
						end
					when 'get'
						begin
							@restclient = RestClient::Resource.new( metadata["callback_url"], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
							@smscresponse = construct_get_params(@smscresponse)
							@call_back_response = 	@restclient.get :params => @smscresponse
							report_call(metadata["calluid"],metadata["to"],"hangup", metadata["callback_url"], @smscresponse, @call_back_response)
						rescue Exception => e
							logger.error "Error while posting hangup to #{metadata['callback_url']}"
							logger.error e
							logger.error e.backtrace
							report_call(metadata["calluid"],metadata["to"],"hangup", metadata["callback_url"], @smscresponse, e)
						end
					end
					if(metadata['user_id'].nil?)
						_user_id = '0'
					else
						_user_id=metadata['user_id']
					end
					@usage_response={"user_id"=>_user_id ,"calluid"=>metadata["calluid"] ,"starttime"=> start_time  , "endtime"=> end_time , "event"=>"hangup" ,"callstatus"=>_callstatus ,"endreason"=>gateway_reason,  "from_number"=>metadata["from"],"to_number"=> metadata["to"]}
					begin
			  			@restclient = RestClient::Resource.new( USAGE_CALLBACK,  :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
			   			@restclient.post  @usage_response , :content_type =>:json 	
			   		rescue Exception => e
			   			logger.info "Error - #{e} - while posting to - #{USAGE_CALLBACK}"
			   		end
				end
				total_duration = end_time-start_time		
			rescue Exception => e
				logger.info("!!!Exception raised in post call processing ->#{e}")
				$redis.publish "outbound_dialer", ({"name"=>"exception","id"=>outbound_call.id , "where"=>"post_obd","exception"=>"!!!Exception raised in post call processing ->#{e}"}).to_json
			end	
		end #call end processing 
		#######################	   
		logger.info "outbound_call data is #{outbound_call.inspect} "
		if metadata["missed_call"] 
			logger.info "missed call attempt to #{metadata["to"]}  with #{metadata["missed_call_duration"]} second ringing"
			sleep(metadata["missed_call_duration"])			
			outbound_call.hangup
		end
		return outbound_call.id
	end
	def	record_invitation_message(contact_type, contact_no,invitation_id , session_secret ,callback_url,metadata )
		start_time = Time.now.to_i
		outbound_call = Adhearsion::OutboundCall.originate "#{get_gateway_prefix(contact_type, contact_no) + contact_no}", from:'smscountry' ,headers:{originate_timeout:ORIGINATE_TIMEOUT}  do
			answer
			answering_time = Time.now.to_i
			data = {"name"=>"at_answer","answering_time"=>answering_time }
			$redis = Redis.new
			$redis.publish "message_recorder", data.to_json
			$redis.quit
			play "/var/punchblock/template/greeting_before_recording.mp3"
			play "/var/punchblock/template/beep.mp3"
			record_result = record start_beep:false, max_duration: 300, interruptible:'*'
			logger.info "Recording saved to #{record_result.recording_uri} and invitation_id is #{invitation_id} "
			play "/var/punchblock/template/farewell_after_recording.mp3"	
			# informing openshift about about file path
			RestClient.get  "#{WEBSITE_URL}/notify_recording_complete?invitation_id=#{invitation_id}&audio_file_path=#{record_result.recording_uri.gsub(/file:\/\//,'')}" 	
			logger.info 	"updated openshift database"
			RestClient.post callback_url, :action=>'recording_complete', :audio_name=>record_result.recording_uri.match(/[^\/]*$/).to_s.gsub(/.wav/,''), :audio_codec=>'wav'  ,:session_secret => session_secret
			`cp  #{record_result.recording_uri.gsub(/file:\/\//,'')} #{record_result.recording_uri.gsub(/file:\/\//,'').gsub(/\.wav/,'_copy.wav')}`
			`sox   #{record_result.recording_uri.gsub(/file:\/\//,'').gsub(/\.wav/,'_copy.wav')}   #{record_result.recording_uri.gsub(/file:\/\//,'').gsub(/\.wav/, '.ogg')}`
			# live update on customer browser using pusher api
		  	_aws_local = Aws.new()	
		  	_aws_local.push_recording(record_result.recording_uri.match(/[^\/]*$/).to_s.gsub(/.wav/,'.ogg'), record_result.recording_uri.gsub(/file:\/\//,'').gsub(/.wav/,'.ogg'))
			_aws_local = nil
			#Pusher['360invite'].trigger('recording_complete', {:invitation_id => invitation_id, :audio_file_path=>record_result.recording_uri.match(/[^\/]*$/).to_s.gsub(/.wav/,'.ogg'),:audio_name=>record_result.recording_uri.match(/[^\/]*$/).to_s.gsub(/.wav/,''), :audio_codec=>'wav'  ,:session_secret => session_secret  })
			logger.info "uploading  #{record_result.recording_uri.gsub(/file:\/\//,'')} to  amazon repository "      
	 		hangup    
		end
		outbound_call.on_end do
	        end_time = Time.now.to_i
	        total_duration = end_time-start_time
			logger.info("call has ended and end reason was #{outbound_call.end_reason}")
			$redis = Redis.new
	        data = {"name"=>'at_end', "start_time"=> start_time,"end_time"=>end_time  , "id"=>outbound_call.id, "end_reason"=>outbound_call.end_reason}
	        $redis.publish "message_recorder", data.to_json
	        $redis.quit	
			#Pusher['360invite'].trigger('at_end',data.merge( { :session_secret => session_secret  }  ))
			@restclient = RestClient::Resource.new( callback_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
	       	@restclient.post callback_url, :action=>'at_end', :session_secret => session_secret, :start_time=>start_time, :end_time=>end_time, :end_reason=>outbound_call.end_reason
        end
        return outbound_call.id
	end
	def tts(invitation_id, text,male_or_female,filename,session_secret,callback_url ,metadata)
		ivona = Ivona.new("/var/punchblock/record/")
		ivona.text_to_ogg(text,male_or_female,filename)
		# k there is a workaround now, instead of ogg we will originally download .mp3
		#convert to ogg and upload on aws, and re
		full_path = @@repo_dir + filename
		`cp #{full_path} #{full_path.gsub(/\.mp3/,'_copy.mp3')}`
		`ffmpeg -i #{full_path.gsub(/\.mp3/,'_copy.mp3')} -acodec libvorbis #{full_path.gsub(/\.mp3/,'_copy.mp3').gsub(/\.mp3/,'.ogg')}  `
		`mv  #{full_path.gsub(/\.mp3/,'_copy.ogg')}  #{full_path.gsub(/\.mp3/,'_copy.ogg').gsub(/_copy\.ogg/,'.ogg')}  `
		@@aws.push_recording( filename.gsub(/\.mp3/,'.ogg'),full_path.gsub(/\.mp3/,'.ogg')  )
		logger.info "uploading  #{full_path} to  amazon repository " 
		#live update on customer browser using pusher api, send file name
		#Pusher['360invite'].trigger('recording_complete', {:invitation_id => invitation_id, :audio_file_path=>filename.gsub(/\.mp3/,'.ogg'),:audio_name=>filename.gsub(/\.mp3/,''),:audio_codec=>'mp3'   , :session_secret =>session_secret   })
		@restclient = RestClient::Resource.new( callback_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
		@restclient.post  :action=>'recording_complete', :audio_name=>filename.gsub(/\.mp3/,''),:audio_codec=>'mp3'   , :session_secret =>session_secret 
	end
	def add_controller(_data)
		_ivm = JSON.parse(_data)
		if !File.exist?(@lib_dir+'/'+_ivm['name'] +'.rb' )
			if system('cd #{@lib_dir}')
				if system('ahn generate controller ' + _ivm['name'])
					puts "controller #{_ivm['name']} generated"
				end
			end
		end
		puts 'controller file exists'
		f = File::open(@lib_dir+'/'+_ivm['name']+'.rb','w')
		if(f)
			puts 'successfully opened controller file for editing'
			f.write("# encoding: utf-8\n
			class #{_ivm['name'].capitalize} < Adhearsion::CallController\ndef run\nanswer\n")
			add_controller_helper(JSON.parse(_ivm['ivm']),f,1)
			f.write("end\nend")
			f.close()
			# once a new controller is generated or existing controller modified we should re-include it
			puts "updating controller repository"	
			require @lib_dir+'/'+_ivm['name']
		end
	end
	private
	
	def add_controller_helper(_data, f , level)
		puts _data
		if _data['total'] == 0
			return
		end
		ivm = _data
		#f.write("# #{ivm['id']}\n")
		for i in 1..ivm['total']
			task = ivm[i.to_s()]
			marker='start'
			case ivm[i.to_s()]['type']
			when 'say'
				level.times do f.write("\t") end
				f.write("say \"#{task['msg']}\"\n")
			when 'ask'
				level.times do f.write("\t") end
				if task['method']=='digit'
					f.write("#{task['var']} = ask \"#{task['msg']}\" , :limit=>#{task['method_var']} , :timeout=>#{@@ask_timeout}\n")
				else
					f.write("#{task['var']} = ask \"#{task['msg']}\" , :terminator=>'#{task['method_var']}' , :timeout=>#{@@ask_timeout}\n")
				end
			when 'menu'
				level.times do f.write("\t") end
				f.write("menu \"#{task['msg']}\" , :timeout=>#{@@menu_timeout}.seconds , :tries=>#{@@menu_tries} do\n")
				for j in 1..task['total']
					f.write("# #{task['list'][j.to_s()]['id']}  ,  #{task['list'][j.to_s()]['desc']}\n")
					(level).times do f.write("\t") end
					f.write("\tmatch #{task['list'][j.to_s()]['press']} do |x|\n")
					add_controller_helper(task['list'][j.to_s()]['start'] , f,level+2)
					level.times do f.write("\t") end	
					f.write("\tend\n")
				end
				level.times do f.write("\t") end
				f.write("end\n")
			else
			end
		end
	end
end
def report_call(_calluid,_to,_event,_posting_url,_posting_data,_response_data)
	logger.info "============ Reporting #{_event} To #{CALL_REPORT_URL} ============"
	@call_reporter = RestClient::Resource.new(CALL_REPORT_URL, :timeout => HTTP_TIMEOUT, :open_timeout => HTTP_OPEN_TIMEOUT)
	@call_reporter.post "calluid" => _calluid, "to" => _to, "event" => _event, "posting_url" => _posting_url, "posting_data" => _posting_data, "response_data" => _response_data
	logger.info "============ Reporting Done ==============="
end
def construct_get_params(_prev_hash)
	_final_params = {}
	_prev_hash.each do |p|
		_final_params["smscresponse[#{p[0].to_s.strip}]"] = p[1].to_s.strip
	end
	return _final_params
end
