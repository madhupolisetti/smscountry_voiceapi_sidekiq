# encoding: utf-8

require 'bundler'
Bundler.setup
Bundler.require
ADHEARSION_APP_ROOT =File.expand_path( File.dirname(__FILE__) + '/..'  ) 
INVITATION_MANAGER_URI = "druby://127.0.0.1:8789"
SIP_TRUNK = 'sofia/gateway/trunk/'
RECORDING_REPO = '/var/punchblock/record/'
TEMPLATE_DIR='/var/punchblock/template/'
PUSHER_APP_ID='39588'
PUSHER_KEY='526471d111e45a27d382'
PUSHER_SECRET='87c3c2e62fbca109501c'
DRB_SERVER_URL='druby://127.0.0.1:8789'
WEBSITE_URL='http://demo-smscountry.rhcloud.com'
REPO_IP = '183.82.2.22'
SINATRA_URL = 'http://183.82.2.22:90'

MINIMUM_SUCCESSFUL_CALL_DURATION=3
ORIGINATE_TIMEOUT=30
CALL_TIMEOUT=10
LEG_TIMEOUT=20
RETRY_LIMIT=0
RETRY_DELAY=1
HTTP_TIMEOUT=60
HTTP_OPEN_TIMEOUT=120
#USAGE_CALLBACK="182.18.147.180:3000/reports_callback"
#USAGE_CALLBACK="https://voiceapi.smscountry.com/reports_callback" #"http://api.tringcast.com/reports_callback"
USAGE_CALLBACK="http://voiceapi.smscountry.com/reports_callback"
CALL_REPORT_URL = "http://voiceapi.smscountry.com/report_call"
