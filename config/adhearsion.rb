# encoding: utf-8
require 'drb/drb'
Adhearsion.config do |config|

  # Centralized way to specify any Adhearsion platform or plugin configuration
  # - Execute rake config:show to view the active configuration values
  #
  # To update a plugin configuration you can write either:
  #
  #    * Option 1
  #        Adhearsion.config.<plugin-name> do |config|
  #          config.<key> = <value>
  #        end
  #
  #    * Option 2
  #        Adhearsion.config do |config|
  #          config.<plugin-name>.<key> = <value>
  #        end

  config.development do |dev|
    dev.platform.logging.level = :debug
  end

  ##
  # Use with Rayo (eg Voxeo PRISM)
  #
  # config.punchblock.username = "" # Your XMPP JID for use with Rayo
  # config.punchblock.password = "" # Your XMPP password

  ##
  # Use with Asterisk
  #
  # config.punchblock.platform = :asterisk # Use Asterisk
  # config.punchblock.username = "" # Your AMI username
  # config.punchblock.password = "" # Your AMI password
  # config.punchblock.host = "127.0.0.1" # Your AMI host

   config.punchblock.platform = :freeswitch # Use FreeSWITCH
   config.punchblock.password = "abc123*" # Your Inbound EventSocket password
   config.punchblock.host = "127.0.0.1" # Your IES host
   config.punchblock.port = "8021"
#   config.punchblock.media_engine = :flite
 #  config.punchblock.default_voice = "kal"

end

Adhearsion::Events.draw do

  # Register global handlers for events
  #
  # eg. Handling Punchblock events
 #  punchblock do |after_initialized|
   
#end
  #
  # eg Handling PeerStatus AMI events
  # ami :name => 'PeerStatus' do |event|
  #   ...
  # end
  #
  # after initialization we need a DRB server running that will cater to out requests
   after_initialized do |event|
#	system('ruby ~/adhearsion/ivr/drb_server.rb')	








require_relative './environment.rb'
require_relative '../drb/invitation_manager.rb'
# The object that handles requests on the server

FRONT_INVITATION_MANAGER = InvitationManager.new()


DRb.start_service(INVITATION_MANAGER_URI, FRONT_INVITATION_MANAGER)
# Wait for the drb server thread to finish before exiting.
DRb.thread.join

  end
  
  
  
  
end

Adhearsion.router do

  #
  # Specify your call routes, directing calls with particular attributes to a controller
  #

#  route 'default', SimonGame
route 'default' ,Incoming 
end
