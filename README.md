# API specification


## Client Callback URL:
```
Incoming: http://202.62.85.170/did  , configure callback url for each DID.
Outgoing: api-smscountry.rhcloud.com/conf , callback url field
```

## Making an Outbound Call:

```xml
/api?xml=
<request>
<from>04030990555</from>
<to>00919989820911</to>
<play duration="30">http://www.360invite.com/voice_clips/greeting1.mp3</play>
<speak duration="30">Thank you for using SMSCountry IVR services</speak>
</request>
```
```json
{"smscresponse" : {
					"calluid" : "123456",
					"to" : "00919989820911",
					"callstatus" : "received",
					"event" : "newcall",
					"direction" : "outbound"
				  }
}
```


## Play Audio/Text:

```xml
/api?xml=
<request>
<play duration="30">http://www.360invite.com/voice_clips/greeting1.mp3</play>
<speak duration="30">Thank you for using SMSCountry IVR services</speak>
</request>
```

## Record:
```xml
/api?xml=
<request>
<speak>Please record your message after the beep.</speak>
<record action="http://www.360invite.com/recordclip.aspx" method="get/post" maxlength="30" timeout="5" fileformat=".wav" finishonkey="#"></record>
<speak>We did not receive any clip.</speak>
</request>
```
```json
{"smscresponse: :{
					"calluid" : "123456",
					"to" : "00919989820911",
					"event" : "record",
					"callstatus" : "inprogress",
					"recordurl" : "http://voice.smscountry.com/recorded.mp3",
					"duration" : "30",
					"direction" : "outbound"
				 }
}
```

## Getting DTMF Keys:

```xml
/api?xml=
<request>
<getkeys action="http://www.360invite.com/savekeys.aspx" method="GET/POST" validkeys="123" max="2" tkey="#">
	<play duration="30">http://www.360invite.com/voice_clips/askfortpin.mp3</play>
</getkeys>
</request>

###Or

<request>
<getkeys action='http://vendor.360invite.com/Plivoincomingcall.aspx' validkeys="123">
<Play>http://vendor.360invite.com/voice_clips/Greeting3.wav</Play>
<Play>http://vendor.360invite.com/voice_clips/Greeting4.wav</Play>
</getkeys>
</request>

```

```json
{"smscresponse" : {
					"calluid" : "123456",
					"to" : "00919989820911",
					"event" : "getkeys",
					"callstatus" : "inprogress",
					"digits" : "3",
					"direction" : "outbound"
				  }
}
```


### Note: Make sure both things work, for Getkeys. As there is a scenario where user can press the key as soon as he/she hears the first instuction/option before listening the second option in that case we need to stop the play and go on with the option/key press done by the user. 

## Hangup:

```xml
/api?xml=
<request>
<hangup><hangup/>
</request>
```

```json
{"smscresponse" : {
					"calluid" : "123456",
					"to" : "00919989820911",
					"event" : "hangup",
					"callstatus" : "completed",
					"direction" : "outbound",
					"starttime" : "Apr-12-2013 15:30:00PM"
					"endtime" : "Apr-12-2013 15:35:00PM"
				  }

}
```

### Incase of incoming call response will appear at callback defined against DID
```json
{"smscresponse" : {
					"calluid" : "123457",
					"from" : "00919989820911",
					"callstatus" : "ringing",
					"event" : "newcall",
					"direction" : "inbound"
				  }
}
```


## Call forward:
### Scenario: Suppose some client have dedicated number(from our side) and wants all the calls landing on this particular number to some of his own internal number
```xml
<request>
<dial duration="30" timeout=”30” action="domainname.aspx"  method=”get/post” from=”47475501”>to_number</dial>
</request>
```

Response:
status="busy/reject/error" if dial failed
status =”answer” if it connects 
we can specify <xml> action items if possible else the connected call continues till it gets hanged up or the specified amount of duration.
After getting hanged up. The possible response parameters on the action url:
direction, start_time, end_time, caller_number, callee_number

### Dial TAG with multiple numbers seperated with comma(,): 
If we specify multiple to_numbers seperated by comma(,), the present active call should connect to one of the to_numbers who-ever Answers first, in dial callback response 
we need to have the particular to_number who picks the call in addition to the present parameters which you are posting currently..


## Async Recording with Dial TAG
### Scenario : If a client want to record  the call session while dialing 
```xml
<request>
<dial duration="30" timeout="30" action="callbackurl" method="get/post" from="XXXXX" record="true/false">XXXXXXXXX</dial>
</request>
```

Response:
A async-recording enabled dial will generate one extra event called "async_record" which is posted with recorded URL 
on to the callbackurl.

## Async Recording Inbound/Outbound Call

Below is the sample outbound generated from API end with call recording enabled.
```xml
/api?xml=
<request recordcall="true" action="http://www.yourdomain.com/callback_report">
<from>04030990555</from>
<to>00919989820911</to>
<play duration="30">http://www.360invite.com/voice_clips/greeting1.mp3</play>
<speak duration="30">Thank you for using SMSCountry IVR services</speak>
</request>
```

For the Inbound, consider the recordcall only if the response from the newcall event gets as below.

```xml
<eventresponse recordcall="true">
<play duration="30">http://www.360invite.com/voice_clips/greeting1.mp3</play>
<speak duration="30">Thank you for using SMSCountry IVR services</speak>
<eventresponse>
```

Response:
An Async Recording for inbound/outbound call will generate event called "record_call" which is posted with recorded URL
on to the callbackurl.


## Conference

```xml
<conference>conference_room_name</conference>
```

### Sample Conference:

```xml
<eventresponse>
	<conference>
		1234
	</conference>
</eventresponse>
```

### Moderated Conference:
	
```xml
Example1
<eventresponse>
	<conference startconferenceonenter="false" waitsound="http://yourdomain.com/waitmusic">
		1234
	</conference>
</eventresponse>

Example2
<eventresponse>
	<conference startconferenceonenter="true" endconferenceonexit="true">
		1234
	</conference>
</eventresponse>
```

### Join a Conference Muted:

```xml
<eventresponse>
	<conference muted="true">
		1234
	</conference>
</eventresponse>
```

### Bridging Two Incoming calls into Conference:

```xml
<eventresponse>
	<conference beep="false" waitsound="http://yourdomain.com/waitmusic" startconferenceonenter="true" endconferenceonexit="true">
		1234		
	</conference>
</eventresponse>
```

### Call On Hold Later Joined/answered by agent:

```xml
<eventresponse>
	<conference waitsound="http://yourdomain.com/waitmusic" beep="false">
		Customer Waiting Room
	</conference>
</eventresponse>

When the Agent Enters into this conference it will be having the attrribute like endCoferenceOnExit="true"

<eventresponse>
	<conference beep="false" endconferenceonexit="true" beep="false">
		Customer Waiting Room
	</conference>
</eventresponse>
```

### Example Of Timelimit with Conference:

```xml
<eventresponse>
	<conference timelimit="30">Timed Conference</conference>
</eventresponse>

Note: here timelimit is 30 Seconds ( i.e., timelimit is in seconds)

```



### Using Callback URL for the Conference:

```xml
<eventresponse>
	<conference callbackurl="http://yourdomain.com/conf_callback" callbackmethod="POST" digitsmatch="123456">
		My Conference Room
	</conference>
</eventresponse>
```


### Callback Request Parameters:

ConferenceAction: "Enter" when user enters the conference, "Digits" when user matches the matching digits 
				   And record if the conference is being recorded using record attribute.

ConferenceName:  Conference Room Name

ConferenceUUID: Unique ID generated for the conference

ConferenceMemberID: ID of the Call in the Conference (do not present if the conferenceaction is record)

CallUUID: Unique Identifier for this Call (do not present if the conferenceaction is record)

ConferenceDigitsMatch: Sent if the conferenceaction is "digits" Digits pattern matching when the member(call) have prtessed the digits.

RecordURL: Sent when the ConferenceAction is record attribute, with the URL of the recorded File.

RecordDuration: Send if the ConferenceAction is record attribute, duration in seconds.


Action Request Parameters (if some one specifies the URL for particular number or if the initial eventresponse TAG have the action attribute):

ConferenceName: Name of the Conference Room

ConferenceUUID: UniqueID of the conference

ConferenceMemberID: Member ID in the Conference

RecordUrl: URL of the recorded conference, only if the record attribute is set to "true".

### Attributes Used In Conference:

muted,
enterSound,
exitSound,
startConferenceOnEnter,
endConferenceOnExit,
waitSound,
Maxmembers,
record,
recordFileFormat,
timeLimit,
callbackUrl,
callbackMethod,
digitsMatch