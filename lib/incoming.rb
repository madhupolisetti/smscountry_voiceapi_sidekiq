# encoding: utf-8
require 'data_mapper'
require_relative '../db/model/did.rb'
require 'ruby_events/core'
require 'rest_client'
require 'pusher'
require 'celluloid'
require_relative '../media/aws.rb'
require_relative '../tts/ivona.rb'
require 'redis'
require 'json'
require 'digest/sha1'
require 'digest/md5'
require 'net/http'

DataMapper.setup(
 :default,  "sqlite://#{File.expand_path(File.dirname(__FILE__))}/../db/360invite.sqlite3.db"
)

DataMapper.auto_upgrade!


class DialWithAsyncRecord < Adhearsion::CallController
	def run
		answer
		logger.info self.inspect		
		record_attr = (!metadata["child"].attributes["record"].nil? && metadata["child"].attributes["record"].value.to_s.strip != "") ? metadata["child"].attributes["record"].value.to_s.strip : "missing"
		if record_attr.downcase=='true' && record_attr!="missing"
			logger.info "Dial is recording enabled"
			$async_record_start_time = Time.now.to_i
			record async: true  do |event|
				recordurl = "#{SINATRA_URL}/media/"+event.recording.uri.match(/[^\/]*$/)[0].to_s
				@smscresponse= {"calluid"=>metadata["metadata"]["calluid"],
								"from"=> metadata["metadata"]["from"],
								"to"=>metadata["metadata"]["to"],
								"event"=>"async_ecord",
								"recordurl"=> recordurl ,
								"starttime"=>$async_record_start_time,
								"endtime"=>Time.now.to_i,
								"callstatus" => "inprogress",
								"callee"=>metadata["child"].text,
								"caller"=>(!metadata["child"].attributes["from"].nil? && metadata["child"].attributes["from"].value.to_s.strip != "") ? metadata["child"].attributes["from"].value.to_s : metadata["metadata"]["from"],
								"direction"=>"inbound"
							}
				async_record_action_url = (!metadata["child"].attributes["action"].nil? && metadata["child"].attributes["action"].value.to_s.strip != "") ? metadata["child"].attributes["action"].value : metadata["metadata"]["callback_url"]
				async_record_action_method = (!metadata["child"].attributes["method"].nil? && metadata["child"].attributes["method"].value.to_s.strip != "") ? metadata["child"].attributes["method"].value : metadata["metadata"]["default_method"]
				@restclient = RestClient::Resource.new(async_record_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
				case async_record_action_method.downcase
				when 'post'
					begin
						@call_back_response =  @restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json						
					rescue Exception => e
						logger.error "Error while post async_record to #{async_record_action_url}"
						logger.error e
						logger.error e.backtrace						
						hangup
					end
				when 'get'
					begin
						@smscresponse = construct_get_params(@smscresponse)
						@call_back_response =   @restclient.get :params => @smscresponse						
					rescue Exception => e
						logger.error "Error while post async_record to #{async_record_action_url}"
						logger.error e
						logger.error e.backtrace						
						hangup
					end
				end
			end
		end
	end
end

class Incoming < Adhearsion::CallController
	@@gateway =SIP_TRUNK
	@@repo_dir = RECORDING_REPO
	@@template_dir = TEMPLATE_DIR
	@@app_dir = ADHEARSION_APP_ROOT 
	@@lib_dir = @@app_dir<<'/lib'
	@@ask_timeout = 100
	@@menu_timeout = 60 #seconds
	@@menu_tries = 3
	@@aws = Aws.new()
	@@hangup_sent = 'no'
	Pusher.app_id = PUSHER_APP_ID 
	Pusher.key = PUSHER_KEY
	Pusher.secret = PUSHER_SECRET
	def get_gateway_prefix(contact_type, contact_no)
		if(contact_type=='pstn')
			@ret = SIP_TRUNK 
		elsif(contact_type=='sip')
			@ret = 'user/'
		end
		if(contact_no[0] == '0' && contact_no[1]== '0')
			#do nothing
		elsif(contact_no[0] == '+')
			#do nothing
		elsif(contact_no[0] != '0' && contact_type == 'pstn' )
			@ret += '0'
		end
		@ret
	end	

	def local_audio_copy(invitation_audio_file)
		_file_name = invitation_audio_file
		if ( invitation_audio_file.include? REPO_IP) # skipping downloading recording file available on this server
			_file_name = invitation_audio_file.match(/[^\/]*$/).to_s
			logger.info("audio file available in media repository  #{_file_name}")
		elsif ( invitation_audio_file.include? "http://")
			_file_name = invitation_audio_file.gsub(/[^a-zA-Z\.0-9]+/, '_')
			if !(File.exist?(@@repo_dir + _file_name))
				`wget -O #{@@repo_dir}#{_file_name}  #{invitation_audio_file  }`
			end
		end
		return _file_name
	end
	def local_tts_copy(txt,male_female)
		logger.info "local_tts_copy outside run method"
		_file_name =    Digest::SHA1.hexdigest(txt)  + ".mp3"
		if !(File.exist?(@@repo_dir + _file_name))
                #ivona = Ivona.new("/var/punchblock/record/")
                #ivona.text_to_ogg(txt,male_female,_file_name)
			_engine_id = "3"
	        _language_id = "1"
        	_voice_id = "3"
	        _account = "3623895"
	        _api_key = "2307281"
	        _secret_phrace = "abb3285edad9861b34074386fc1eb8c5"
	        _cs = ""
	        _post_md5 = ""
	        _base_url = "http://cache.oddcast.com/tts/gen.php"
	        _post_info = ""
	        _post_info = "?EID=" + _engine_id.to_s + "&LID=" + _language_id.to_s + "&VID=" + _voice_id.to_s + "&TXT=" + URI.encode(txt).to_s + "&ACC=" + _account.to_s + "&API=" + _api_key.to_s
	       _post_md5 = _engine_id.to_s + _language_id.to_s + _voice_id.to_s + txt.to_s + _account.to_s + _api_key.to_s + _secret_phrace.to_s
	        _cs = Digest::MD5.hexdigest(_post_md5) #"3" + "1" + "3" + "This is for a sample test from madhu please ignore it." + "3623895" + "2307281" + "abb3285edad9861b34074386fc1eb8c5"        
	        _post_info = _post_info + "&CS=" + _cs
	        begin
				logger.info "vocalware api call started"
        	    uri = URI(_base_url + _post_info)
	            res = Net::HTTP.get_response(uri)
				logger.info "vocal ware api call ended"
				File.new(@@repo_dir + _file_name, "w+")
				File.open(@@repo_dir + _file_name, 'w') { |file| file.write(res.body) }
				logger.info "file saved to " + @@repo_dir + _file_name
        	rescue Exception => e
				logger.info "Exception in vocalware api call"
	            logger.info e.backtrace
        	end
		end
		logger.info "internal tts job done #{_file_name}"
        return _file_name
	end
	def run
		start_time = Time.now.to_i.to_s
		to_number = call.to.gsub(/@.*$/,'')
		logger.info "********************* incoming call for #{to_number}"
		#@did = JSON.parse(RestClient.get("http://voiceapi.smscountry.com/did_metadata/" + to_number  )) 
		if to_number.include?"5560"
			sleep(6)
		end
		if to_number == "5500" || to_number =="39363900" || to_number =="30990555"
			@did = Did.first(:did=>"01")
		elsif to_number.include? "3936394"
			@did = Did.first(:did=>"96")
		else
			@did = Did.first(:did=>to_number.gsub(/474755/,""))
		end
		if @did	
			metadata = {"to"=>to_number,
						"from"=>call.from.match(/<.*>/).to_s.gsub(/<|@.*$/, ''),
						"calluid"=>SecureRandom.uuid.to_s + "-" + Time.now.to_i.to_s + "i",
						"callback_url"=> @did['callback']
					}
			logger.info "metadata is #{metadata}"
			def local_audio_copy(invitation_audio_file)
				_file_name = invitation_audio_file
				if ( invitation_audio_file.include? REPO_IP) # skipping downloading recording file available on this server					
					_file_name = invitation_audio_file.match(/[^\/]*$/).to_s
					logger.info("audio file available in media repository  #{_file_name}")
				elsif ( invitation_audio_file.include? "http://")
					_file_name = invitation_audio_file.gsub(/[^a-zA-Z\.0-9]+/, '_')
					if !(File.exist?(@@repo_dir + _file_name))
						`wget -O #{@@repo_dir}#{_file_name}  #{invitation_audio_file  }`
					end
				end
				return _file_name 
			end

			def local_tts_copy(txt,male_female)
				logger.info "local_tts_copy inside the run method"
				# _file_name =    Digest::SHA1.hexdigest(txt)  + ".mp3"
				# if !(File.exist?(@@repo_dir + _file_name))
				# 	ivona = Ivona.new("/var/punchblock/record/")
				# 	ivona.text_to_ogg(txt,male_female,_file_name)
				# end
				# logger.info "internal tts job done #{_file_name}"
				# return _file_name
				_file_name =    Digest::SHA1.hexdigest(txt)  + ".mp3"
				if !(File.exist?(@@repo_dir + _file_name))
		                #ivona = Ivona.new("/var/punchblock/record/")
		                #ivona.text_to_ogg(txt,male_female,_file_name)
					_engine_id = "3"
			        _language_id = "1"
		        	_voice_id = "3"
			        _account = "3623895"
			        _api_key = "2307281"
			        _secret_phrace = "abb3285edad9861b34074386fc1eb8c5"
			        _cs = ""
			        _post_md5 = ""
			        _base_url = "http://cache.oddcast.com/tts/gen.php"
			        _post_info = ""
			        _post_info = "?EID=" + _engine_id.to_s + "&LID=" + _language_id.to_s + "&VID=" + _voice_id.to_s + "&TXT=" + URI.encode(txt).to_s + "&ACC=" + _account.to_s + "&API=" + _api_key.to_s
			       _post_md5 = _engine_id.to_s + _language_id.to_s + _voice_id.to_s + txt.to_s + _account.to_s + _api_key.to_s + _secret_phrace.to_s
			        _cs = Digest::MD5.hexdigest(_post_md5) #"3" + "1" + "3" + "This is for a sample test from madhu please ignore it." + "3623895" + "2307281" + "abb3285edad9861b34074386fc1eb8c5"        
			        _post_info = _post_info + "&CS=" + _cs
			        begin
						logger.info "vocalware api call started"
		        	    uri = URI(_base_url + _post_info)
			            res = Net::HTTP.get_response(uri)
						logger.info "vocal ware api call ended"
						File.new(@@repo_dir + _file_name, "w+")
						File.open(@@repo_dir + _file_name, 'w') { |file| file.write(res.body) }
						logger.info "file saved to " + @@repo_dir + _file_name
		        	rescue Exception => e
						logger.info "Exception in vocalware api call"
			            logger.info e.backtrace
		        	end
				end
				logger.info "internal tts job done #{_file_name}"
		        return _file_name
			end

		
			def is_this_a_missed_call(xml)   
				logger.info "missed call diagnostic"
				@doc = Nokogiri::XML(xml)
				@doc.first_element_child.children.each do |child|
					if(child.name == 'to' || child.name =='from' || child.name == 'callback_url' || child.name == 'text' )
						child.remove
					end
				end
				@remaining = @doc.first_element_child.children
				logger.info "remaining is #{@remaining}"				
				if(@remaining.length == 1 && @remaining.first.name == 'hangup')
					return (!@remaining.first.attributes["duration"].nil? && @remaining.first.attributes["duration"].value.to_s.strip != "") ? @remaining.first.attributes["duration"].value.to_s.to_i : 1
				else
					return nil
				end
			end
			##########################
			@xml = @did['xml']			
			@enclosure
			logger.info "#######################################################"+@xml			
			@enclosure = 'register'
			#missed call bit
			@delay = is_this_a_missed_call(@xml)
			if (!@doc.first_element_child.attributes["method"].nil? && @doc.first_element_child.attributes["method"].value.to_s.strip != "")								
				metadata["default_method"] = @doc.first_element_child.attributes['method'].value.to_s.strip
			else
				metadata["default_method"] = "get"
			end
			if  !@delay.nil?
				metadata["missed_call"] =  true
				logger.info "delay is #{@delay}"
				metadata["missed_call_duration"] = @delay.to_s.to_i 
				@smscresponse= {"calluid"=>metadata["calluid"],
								"from"=> metadata["from"],
								"to"=> metadata["to"],
								"event"=>"missed_call_hangup",
								"callstatus" => "completed",
								"direction"=>"inbound" ,
								"starttime"=>start_time,
								"endtime"=>Time.now.to_i.to_s
							}
				case metadata['default_method'].downcase
				when 'post'
					begin
						@restclient = RestClient::Resource.new( metadata["callback_url"], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)	
						@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
					rescue Exception => e
						logger.error "Error while posting missed_call_hangup to #{metadata['callback_url']}"
						logger.error e
						logger.error e.backtrace
					end
				when 'get'
					begin
						@restclient = RestClient::Resource.new( metadata["callback_url"], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
						@smscresponse = construct_get_params(@smscresponse)
						@call_back_response = 	@restclient.get :params => @smscresponse
					rescue Exception => e
						logger.error "Error while posting missed_call_hangup to #{metadata['callback_url']}"
						logger.error e
						logger.error e.backtrace
					end
				end				
			end
			#sleep(6)	
			#sleep(3)
			if(metadata["missed_call"]	) #if missed call hangup 
				sleep(metadata["missed_call_duration"])		
				hangup
			else
				answer
				while(true)
					logger.info "process xml data ========================== #{@xml}  " 
					if !@xml.nil?  && @xml.to_s.strip != ''
						@smscresponse = nil			
						_xml = Nokogiri::XML(@xml)
						@xml = nil #consume xml
						if(_xml.first_element_child().nil?)
							break
						else
							@enclosure = _xml.first_element_child().name							
							c = _xml.first_element_child().children()			
							c.each do |child|
								if(child.name== 'hangup')
									logger.info "hangup..................................................."
									@smscresponse= {"calluid"=>metadata["calluid"],
													"from"=> metadata["from"],
													"to"=> metadata["to"],
													"event"=>"hangup",
													"callstatus" => "completed",
													"direction"=>"inbound",
													"starttime"=>start_time,
													"endtime"=>Time.now.to_i.to_s
												}
									hangup_url = (!child.attributes['action'].nil? && child.attributes['action'].value.to_s.strip != "") ? child.attributes['action'].value.to_s.strip : metadata['callback_url']
									hangup_method = (!child.attributes['method'].nil? && child.attributes['method'].value.to_s.strip != "") ? child.attributes['method'].value.to_s.strip : metadata['default_method']
									@restclient = RestClient::Resource.new(hangup_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)
									case hangup_method.downcase
									when "get"
										begin
											@hangup_sent = 'yes'
											@smscresponse = construct_get_params(@smscresponse)
											@call_back_response = 	@restclient.get :params => @smscresponse
										rescue Exception => e
											@hangup_sent = 'no'
											logger.error "Error while posting hangup to #{hangup_url}"
											logger.error e
											logger.error e.backtrace										
											hangup
										end
									when "post"
										begin
											@@hangup_sent = 'yes'										
											@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
										rescue Exception => e
											@@hangup_sent = 'no'
											logger.error "Error while posting hangup to #{hangup_url}"
											logger.error e
											logger.error e.backtrace
											hangup
										end
									end									
									if (!child.attributes['duration'].nil? && child.attributes['duration'].value.to_s.strip != "")  
										sleep(child.attributes['duration'].value.to_s.to_i)
									end
									hangup
									break
								end#hangup child
								if(child.name== 'play')
									logger.info "play......................................................"
									play @@repo_dir+local_audio_copy(child.text)
								end
								if(child.name== 'speak')
									logger.info "speak....................................................."
									play @@repo_dir+local_tts_copy(child.text, 'female')
								end
								if(child.name== 'dial')
									logger.info "dial......................................................"
									dial_start_time = Time.now.to_i
									moh_clip = (!child.attributes["onholdmusic"].nil? && child.attributes["onholdmusic"].value.to_s.strip != "") ? @@repo_dir + child.attributes["onholdmusic"].value.to_s.strip : @@repo_dir + "moh.wav"
									play! moh_clip
			 						destination=[]
									child.text.split(",").each do |dest|
										destination.push "#{get_gateway_prefix('pstn',dest.strip) + dest.strip}"
									end
									dial_timeout = (!child.attributes["timeout"].nil? && child.attributes["timeout"].value.to_s.strip != "") ? child.attributes["timeout"].value.to_s.to_i : 30
									dial_from = (!child.attributes["from"].nil? && child.attributes["from"].value.to_s.strip != "") ? child.attributes["from"].value : metadata["from"]
									dial_action_url = (!child.attributes["action"].nil? && child.attributes["action"].value.to_s.strip != "") ? child.attributes["action"].value : metadata["callback_url"]
									dial_action_method = (!child.attributes["method"].nil? && child.attributes["method"].value.to_s.strip != "") ? child.attributes["method"].value : metadata["default_method"]
									status = dial destination, for: dial_timeout.to_i.seconds , confirm: DialWithAsyncRecord , confirm_metadata:{"child"=>child, "metadata"=>metadata}	 
									case status.result
   				 					when :answer
				      					@smscresponse= {"calluid"=>metadata["calluid"],
				      									"from"=> metadata["from"],
				      									"to"=>metadata["to"],
				      									"event"=>"dial",
				      									"status"=>"answer" ,
				      									"starttime"=>dial_start_time,
				      									"endtime"=>Time.now.to_i,
				      									"callstatus" => "inprogress",
				      									"callee"=>child.text,
				      									"caller"=>dial_from,
				      									"direction"=>"inbound"
				      								}
				 					when :error, :timeout, :no_answer
										@smscresponse= {"calluid"=>metadata["calluid"],
														"from"=> metadata["from"],
														"to"=>metadata["to"],
														"event"=>"dial",
														"status"=>status.result.to_s,
														"callstatus" => "inprogress",
														"callee"=>child.text,
														"caller"=>dial_from,
														"direction"=>"inbound"
													}
									end				
									case dial_action_method.downcase
									when 'post'
										logger.info "posting for getkeys  #{@smscresponse}"
										begin
											@restclient = RestClient::Resource.new(dial_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
											@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
										rescue Exception => e
											logger.error "Error while posting dial to #{dial_action_url}"
											logger.error e
											logger.error e.backtrace
											hangup
										end
									when 'get'
										begin
											@restclient = RestClient::Resource.new(dial_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
											@smscresponse = construct_get_params(@smscresponse)
											@call_back_response = 	@restclient.get :params => @smscresponse
										rescue Exception => e
											logger.error "Error while posting dial to #{dial_action_url}"
											logger.error e
											logger.error e.backtrace
											hangup
										end
									end
									#	@call_back_response = JSON.parse(@call_back_response)
									logger.info "response for dial  #{@call_back_response}"
									if !@call_back_response.nil? && @call_back_response.to_s.strip!=''
										logger.info "DIAL RESPONSE IS NOT NIL OR BLANK"
										@xml = @call_back_response
										break					
									end
								end#dial child
								if(child.name=='getkeys')
									# processing speak and play
									ask_announcement = []
									ask_timeout={}
									logger.info "getkey xml tree ------------> #{child}"
									child.children.each do |grand_child|
										if(grand_child.name== 'play')
											logger.info "play......................................................#{child.text}"						
											ask_announcement.push @@repo_dir+local_audio_copy(grand_child.text)
										end
										if(grand_child.name== 'speak')
											logger.info "speak....................................................."
											ask_announcement.push @@repo_dir+local_tts_copy(grand_child.text, 'female')
										end					
										if(grand_child.name== 'timeout')
											ask_timeout = { "count" => (!grand_child.attributes["count"].nil? && grand_child.attributes["count"].to_s != "") ? grand_child.attributes['count'].value.to_s.to_i : 1,
															"type" =>   grand_child.attributes['type'].value , 
															"content" => grand_child.text}					
										end				
									end									
									ask_announcement = ask_announcement.push  #",#{@@template_dir}beep.mp3"			
									##########################################
									max_keys = (!child.attributes["max"].nil? && child.attributes["max"].value.to_s.strip != "") ? child.attributes["max"].value.to_s.to_i : 1
									keys_terminator = (!child.attributes["tkey"].nil? && child.attributes["tkey"].value.to_s.strip != "") ? child.attributes["tkey"].value : "#"
									valid_keys = (!child.attributes["validkeys"].nil? && child.attributes["validkeys"].value.to_s.strip != "") ? child.attributes["validkeys"].value : "0123456789*#"
									keys_timeout = (!child.attributes["timeout"].nil? && child.attributes["timeout"].to_s.strip != "") ? child.attributes["timeout"].value.to_s.strip.to_i : 10
									getkeys_action_url = (!child.attributes["action"].nil? && child.attributes["action"].value.to_s.strip != "") ? child.attributes["action"].value.to_s.strip : metadata["callback_url"]
									getkeys_action_method = (!child.attributes["method"].nil? && child.attributes["method"].value.to_s.strip != "") ? child.attributes["method"].value.to_s.strip : metadata["default_method"]
									logger.info "getkeys...................................................Max Keys : #{max_keys} , Terminator : #{keys_terminator}    announcements are #{ask_announcement}  valid keys => #{valid_keys}"				
									for i in 1..((ask_timeout.nil?)? 1 : ask_timeout["count"] ).to_i	
										logger.info "getkeys attempt  -------  #{i} -------"				
										ask_result = ask   ask_announcement, :limit=>max_keys, :terminator=>keys_terminator, :timeout=>keys_timeout				
										if ask_result.nil? && ask_result.response.strip == ''
											if !ask_timeout.nil?						
												ask_timeout["type"]=='speak' ? (play @@repo_dir+local_tts_copy(ask_timeout["content"], 'female')) : (play @@repo_dir+local_audio_copy(ask_timeout["content"]))
											end
										else
				    						break
										end
									end
									_res = (!ask_result.nil? && !ask_result.response.nil?) ? ask_result.response.strip : ""
									if _res != ""
										@smscresponse= {"calluid"=>metadata["calluid"],
													"from"=> metadata["from"],
													"to"=>metadata["to"],
													"event"=>"getkeys",
													"callstatus" => "inprogress",
													"digits"=>_res,
													"direction"=>"inbound"
												}				
										case getkeys_action_method.downcase
										when 'post'
											logger.info "posting for getkeys  #{@smscresponse}"
											begin
												@restclient = RestClient::Resource.new(getkeys_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
												@call_back_response = @restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
											rescue Exception => e
												logger.error "Error while posting getkeys to #{getkeys_action_url}"
												logger.error e
												logger.error e.backtrace											
												hangup							
											end
										when 'get'
											begin
												@restclient = RestClient::Resource.new(getkeys_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
												@smscresponse = construct_get_params(@smscresponse)
												@call_back_response = 	@restclient.get :params => @smscresponse
											rescue Exception => e
												logger.error "Error while posting getkeys to #{getkeys_action_url}"
												logger.error e
												logger.error e.backtrace												
												hangup
											end				
										end
									end									
									#	@call_back_response = JSON.parse(@call_back_response)
									logger.info "response for getkeys  #{@call_back_response}"
									if !@call_back_response.nil? && @call_back_response.to_s.strip != ""
										@xml = @call_back_response
										break
									end
								end#getkeys child
								if(child.name == 'record')
									record_action_url = (!child.attributes["action"].nil? && child.attributes["action"].value.to_s.strip != "") ? child.attributes["action"].value.to_s.strip : metadata["callback_url"]
						 			record_action_method = (!child.attributes["method"].nil? && child.attributes["method"].value.to_s.strip != "") ? child.attributes["method"].value.to_s.strip : metadata["default_method"]
						 			max_length = (!child.attributes["maxlength"].nil? && child.attributes["maxlength"].value.to_s.strip != "") ? child.attributes["maxlength"].value.to_s.strip.to_i : 30
						 			record_terminator = (!child.attributes["finishonkey"].nil? && child.attributes["finishonkey"].value.to_s.strip != "") ? child.attributes["finishonkey"].value.to_s.strip : "#"
									logger.info "record....................................................max-duration => #{ max_length} , interrupt by =>#{record_terminator}"
									play @@template_dir+"long_beep.mp3"
									record_start = Time.now.to_i 
									logger.info "%%%%%%%%%%%%%%%%%%%%%%%%%% recording started %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
									record_result = record  :max_duration=>max_length.to_i, :interruptible=>record_terminator
									logger.info "%%%%%%%%%%%%%%%%%%%%%%%%%% recording ended %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
									record_end = Time.now.to_i
									@smscresponse= {"calluid"=>metadata["calluid"] ,
													"from"=> metadata["from"],
													"to"=>metadata["to"],
													"event"=>"record",
													"callstatus" => "inprogress",
													"recordurl"=>"#{SINATRA_URL}/media/"+record_result.recording_uri.match(/[^\/]*$/).to_s,
													"duration"=>(record_end - record_start).to_s,
													"direction"=>"inbound"
												}
									logger.info "record action url called with #{@smscresponse}"				
									logger.info "record result saved in #{record_result.recording_uri.match(/[^\/]*$/).to_s}"
									logger.info "send result to record action url #{record_action_url}"				
									case record_action_method.downcase
									when 'post'
										begin
											@restclient = RestClient::Resource.new(record_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
											@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json 
										rescue Exception => e
											logger.error "Error while posting record to #{record_action_url}"
											logger.error e
											logger.error e.backtrace
											hangup
										end 
									when 'get'
										begin
											@restclient = RestClient::Resource.new(record_action_url, :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
											@smscresponse = construct_get_params(@smscresponse)
											@call_back_response = 	@restclient.get :params => @smscresponse
										rescue Exception => e
											logger.error "Error while posting record to #{record_action_url}"
											logger.error e
											logger.error e.backtrace
											hangup
										end
									end
									if !@call_back_response.nil? && @call_back_response.to_s.strip != ""
										@xml = @call_back_response
										break
									end			
								end
							end# Record child
						end
						if((@xml.nil? || @xml.to_s.strip=='') && @enclosure=='register')
							logger.info "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ #{metadata}"
							@smscresponse= {"calluid"=>metadata["calluid"],
											"from"=> metadata["from"],
											"to"=>metadata["to"],
											"event"=>"newcall",
											"callstatus" => "ringing",
											"direction"=>"inbound"
										}
							case metadata['default_method'].downcase
							when 'post'
								begin
									@restclient = RestClient::Resource.new(metadata["callback_url"], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
									@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
								rescue Exception => e
									logger.error "Error while posting newcall to #{metadata['callback_url']}"
									logger.error e
									logger.error e.backtrace
									hangup
								end
							when 'get'
								begin
									@restclient = RestClient::Resource.new(metadata['callback_url'], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
									@smscresponse = construct_get_params(@smscresponse)
									@call_back_response = 	@restclient.get :params => @smscresponse
								rescue Exception => e
									logger.error "Error while posting newcall to #{metadata['callback_url']}"
									logger.error e
									logger.error e.backtrace
									hangup
								end
							end
							if !@call_back_response.nil? && @call_back_response.to_s.strip != ""
								@xml = @call_back_response
								logger.info "XML recieved in new-call response : #{@xml}"
							else
								hangup
								break
							end
						elsif(@xml.nil?)
							hangup
							break
						end
					end # non proper xml
				end#while end
				logger.info "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ after call #{@@hangup_sent}"
				if @@hangup_sent == 'no'
					@smscresponse= {"calluid"=>metadata["calluid"],
									"from"=> metadata["from"],
									"to"=> metadata["to"],
									"event"=>"hangup","callstatus" => "completed",
									"direction"=>"inbound",
									"starttime"=>start_time,
									"endtime"=>Time.now.to_i.to_s
								}
					case metadata['default_method'].downcase
					when 'post'
						begin
							@restclient = RestClient::Resource.new(metadata["callback_url"], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
							@call_back_response = 	@restclient.post JSON.pretty_generate({"smscresponse"=> @smscresponse}), :content_type => :json, :accept => :json
						rescue Exception => e
							logger.error "Error while posting hangup to #{metadata['callback_url']}"
							logger.error e
							logger.error e.backtrace
							hangup
						end
					when 'get'
						begin
							@restclient = RestClient::Resource.new(metadata['callback_url'], :timeout => HTTP_TIMEOUT, :open_timeout=>HTTP_OPEN_TIMEOUT)					
							@smscresponse = construct_get_params(@smscresponse)
							@call_back_response = 	@restclient.get :params => @smscresponse
						rescue Exception => e
							logger.error "Error while posting newcall to #{metadata['callback_url']}"
							logger.error e
							logger.error e.backtrace
							hangup
						end
					end
				end
			end#missed call
		end# reliance temporary call routing	
	end
end
def construct_get_params(_prev_hash)
	_final_params = {}
	_prev_hash.each do |p|
		_final_params["smscresponse[#{p[0].to_s.strip}]"] = p[1].to_s.strip
	end
end
